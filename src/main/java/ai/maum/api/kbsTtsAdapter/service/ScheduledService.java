package ai.maum.api.kbsTtsAdapter.service;

import ai.maum.api.kbsTtsAdapter.common.CommonCode;
import ai.maum.api.kbsTtsAdapter.exception.AdapterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class ScheduledService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public void fileRemover() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DATE, - 8);
        Date expDate = calendar.getTime();
        String deletePath = CommonCode.DEFAULT_PATH + simpleDateFormat.format(expDate);
        deleteFolder(deletePath);
    }

    private void deleteFolder(String path) throws AdapterException {
        File deletePath = new File(path);

        if(deletePath.exists()) {
            File[] deleteFiles = deletePath.listFiles();

            for (File file : deleteFiles) {
                if(file.isFile()) {
                    file.delete();
                    logger.info(String.format("File Deleted : %s", file.getName()));
                } else {
                    deleteFolder(file.getPath());
                    logger.info(String.format("Folder Deleted : %s", file.getName()));
                }
                file.delete();
            }
        }
        deletePath.delete();
        logger.info(String.format("Delete Date : %s", path));
    }

}
