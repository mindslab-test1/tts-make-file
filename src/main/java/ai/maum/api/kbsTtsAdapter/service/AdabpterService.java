package ai.maum.api.kbsTtsAdapter.service;

import ai.maum.api.kbsTtsAdapter.exception.AdapterException;
import ai.maum.api.kbsTtsAdapter.model.params.KbsTtsRequestParams;
import ai.maum.api.kbsTtsAdapter.model.response.KbsMakeFileResponse;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static ai.maum.api.kbsTtsAdapter.common.CommonCode.*;

@Service
public class AdabpterService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public KbsMakeFileResponse kbsTtsMakeFile(KbsTtsRequestParams params, HttpServletRequest request) throws AdapterException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd/");
        String dateString = simpleDateFormat.format(new Date());

        logger.info(params.toString());

        auth(params.getApiId());
        checkSpeed(params.getSpeed());

        try {
            String url = API_URL;
            String replaceString = "_";
            String reqName = params.getRequestName().replace(" ", replaceString)
                    .replace("/", replaceString)
                    .replace("\\", replaceString)
                    .replace("?", replaceString)
                    .replace("%", replaceString)
                    .replace("*", replaceString)
                    .replace(":", replaceString)
                    .replace("|", replaceString)
                    .replace("\"", replaceString)
                    .replace("<", replaceString)
                    .replace(">", replaceString)
                    .replace(".", replaceString);

            reqName = String.format("%s_%s_%s_%s", reqName, "mindslab", params.getVoiceName(), getDateString());

            String downloadId;
            if (params.getDownloadId() != null) {
                downloadId = params.getDownloadId();
            } else {
                downloadId = "";
            }


            String filePath = DEFAULT_PATH + dateString + downloadId + "/" + reqName + WAV_SUFFIX;


            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("text", new StringBody(params.getText(), ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiId", new StringBody(params.getApiId(), ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(params.getApiKey(), ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("voiceName", new StringBody(params.getVoiceName(), ContentType.TEXT_PLAIN.withCharset("UTF-8")));

            HttpEntity entity = builder.build();

            post.setEntity(entity);
            HttpResponse httpResponse = client.execute(post);
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            logger.info("responseCode = {}", responseCode);

            if (responseCode != 200) {
                throw new AdapterException(responseCode);
            }

            HttpEntity responseEntity = httpResponse.getEntity();

            InputStream in = responseEntity.getContent();
            byte[] audioArray = IOUtils.toByteArray(in);

            // 서버에 파일 저장
            File ttsFile = new File(DEFAULT_PATH + dateString + downloadId);

            if (!ttsFile.exists()) {
                try {
                    ttsFile.mkdirs();
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(audioArray);
            fos.flush();
            fos.close();

            String tempPath = DEFAULT_PATH + UUID.randomUUID() + WAV_SUFFIX;

            File originalFile = new File(filePath);
            File tempFile = new File(tempPath);
            originalFile.renameTo(tempFile);

            String command = String.format("ffmpeg -i %s -ac 2 -ar 48000 ", tempPath);

            if (params.getSpeed() != 1.0) {
                logger.info(String.format("Convert TTS Speed : %f", params.getSpeed()));
                command += String.format("-filter:a atempo=%f -vn ", params.getSpeed());
            }

            command += filePath;

            Process processor = Runtime.getRuntime().exec(command);
            try {
                processor.waitFor();
            } catch (InterruptedException e) {
                throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            tempFile.delete();
            logger.info(command);

            String urlReq;
            if (downloadId.isEmpty()) {
                urlReq = getDomain(request, reqName);
            } else {
                String downloadReqPath = DEFAULT_PATH + dateString + downloadId;
                downloadReqPath = downloadReqPath.replaceAll("/", "~") + "/";
                urlReq = getDomain(request, downloadReqPath + reqName);
            }

            KbsMakeFileResponse response = new KbsMakeFileResponse(urlReq, getExpDate());
            logger.info(response.toString());
            return response;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            if (e instanceof AdapterException) throw e;
            throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String getDateString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        return simpleDateFormat.format(new Date());
    }

    public void fileDownload(HttpServletRequest request, HttpServletResponse response, String fileId) throws AdapterException {
        File file = new File(DEFAULT_PATH + parseFileId(fileId));

        logger.info("Path received {}", file);
        if (!file.exists()) {
            logger.warn("[KBS TTS FileDownload] : File is not exists " + fileId);
            throw new AdapterException(HttpStatus.BAD_REQUEST);
        }

        FileInputStream fileInputStream = null;
        ServletOutputStream servletOutputStream = null;

        try {
            String downName = null;
            String browser = request.getHeader("User-Agent");
            if (browser.contains("MSIE") || browser.contains("Trident") || browser.contains("Chrome")) {
                downName = URLEncoder.encode(fileId, "UTF-8").replace("\\+", "%20");
            } else {
                downName = new String(fileId.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
            }

            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setContentType("application/octer-stream");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            fileInputStream = new FileInputStream(file);
            servletOutputStream = response.getOutputStream();

            byte[] byteArray = new byte[1024];
            int data = 0;

            while ((data = (fileInputStream.read(byteArray, 0, byteArray.length))) != -1) {
                servletOutputStream.write(byteArray, 0, data);
            }

            servletOutputStream.flush();
            logger.info("[KBS TTS FileDownload Done] : " + fileId);
        } catch (IOException e) {
            throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {

            if (servletOutputStream != null) {
                try {
                    servletOutputStream.close();
                } catch (IOException e) {
                    throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }
    }

    public void fileDownload2(HttpServletRequest request, HttpServletResponse response, String downloadPath, String fileId) throws AdapterException {
        downloadPath = downloadPath.replaceAll("~", "/");
        File file = new File(downloadPath + "/" + fileId);
        logger.info("Path received {}", file);

        if (!file.exists()) {
            logger.warn("[KBS TTS FileDownload] : File is not exists " + fileId);
            throw new AdapterException(HttpStatus.BAD_REQUEST);
        }

        FileInputStream fileInputStream = null;
        ServletOutputStream servletOutputStream = null;

        try {
            String downName = null;
            String browser = request.getHeader("User-Agent");
            if (browser.contains("MSIE") || browser.contains("Trident") || browser.contains("Chrome")) {
                downName = URLEncoder.encode(fileId, "UTF-8").replace("\\+", "%20");
            } else {
                downName = new String(fileId.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
            }

            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            fileInputStream = new FileInputStream(file);
            servletOutputStream = response.getOutputStream();

            byte[] byteArray = new byte[1024];
            int data = 0;

            while ((data = (fileInputStream.read(byteArray, 0, byteArray.length))) != -1) {
                servletOutputStream.write(byteArray, 0, data);
            }

            servletOutputStream.flush();
            logger.info("[KBS TTS FileDownload Done] : " + fileId);
        } catch (IOException e) {
            throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {

            if (servletOutputStream != null) {
                try {
                    servletOutputStream.close();
                } catch (IOException e) {
                    throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    throw new AdapterException(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }
    }

    private String parseFileId(String fileId) {
        String[] fileIdSplit = fileId.split("_");
        SimpleDateFormat requestDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy-MM-dd/");
        try {
            return fileDateFormat.format(requestDateFormat.parse(fileIdSplit[(fileIdSplit.length - 1)])) + fileId;
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
            throw new AdapterException(HttpStatus.BAD_REQUEST);
        }
    }

    private void auth(String apiId) throws AdapterException {
        if (!apiId.equals("swhange91d6f0f28ab") && !apiId.contains("mindslab")) {
            throw new AdapterException(HttpStatus.FORBIDDEN);
        }
    }

    private void checkSpeed(Double speed) {
        if (speed < 0.8 || speed > 1.2) {
            throw new AdapterException(HttpStatus.BAD_REQUEST);
        }
    }

    private String getDomain(HttpServletRequest request, String reqName) {
        // Deployed version should not have port nummber
        return request.getHeader("x-forwarded-proto") + "://" + request.getServerName() + FILE_DOWNLOAD_SUB_URL + "/" + reqName + WAV_SUFFIX;
        // Local testing requires port number
//        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + FILE_DOWNLOAD_SUB_URL + "/" + reqName + WAV_SUFFIX;
    }

    private Date getExpDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, EXPIRY_DATE);
        return calendar.getTime();
    }

}