package ai.maum.api.kbsTtsAdapter.model.response;

import ai.maum.api.kbsTtsAdapter.exception.AdapterException;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class KbsExceptionResponse {
    private String result = "Fail";
    private int status;
    private String message;

    public KbsExceptionResponse(AdapterException exception) {
        this.status = exception.getStatus();
        this.message = exception.getMessage();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "KbsExceptionResponse{" +
                "result='" + result + '\'' +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
