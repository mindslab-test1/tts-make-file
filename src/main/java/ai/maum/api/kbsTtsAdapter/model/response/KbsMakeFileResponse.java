package ai.maum.api.kbsTtsAdapter.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.text.SimpleDateFormat;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class KbsMakeFileResponse {

    private String result = "Success";
    private int status = 200;
    private String downloadURL;
    private String expiryDate;

    public KbsMakeFileResponse() {
    }

    public KbsMakeFileResponse(String downloadURL, Date expiryDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        this.downloadURL = downloadURL;
        this.expiryDate = simpleDateFormat.format(expiryDate);
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "KbsMakeFileResponse{" +
                "result='" + result + '\'' +
                ", status=" + status +
                ", downloadURL='" + downloadURL + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                '}';
    }
}
