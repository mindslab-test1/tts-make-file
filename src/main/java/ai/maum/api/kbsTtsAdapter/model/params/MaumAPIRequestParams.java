package ai.maum.api.kbsTtsAdapter.model.params;

public class MaumAPIRequestParams {
    private String apiId;
    private String apiKey;
    private String text;
    private String voiceName;

    public MaumAPIRequestParams(String apiId, String apiKey, String text, String voiceName) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.text = text;
        this.voiceName = voiceName;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    @Override
    public String toString() {
        return "MaumAPIRequestParams{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", text='" + text + '\'' +
                ", voiceName='" + voiceName + '\'' +
                '}';
    }
}
