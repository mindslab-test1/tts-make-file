package ai.maum.api.kbsTtsAdapter.model.params;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class KbsTtsRequestParams {

    private String apiId;
    private String apiKey;
    private String text;
    private String voiceName;
    private Double speed = 1.0;
    private String requestName;
    private String downloadId;

    @Override
    public String toString() {
        return "KbsTtsRequestParams{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", text='" + text + '\'' +
                ", voiceName='" + voiceName + '\'' +
                ", speed=" + speed +
                ", requestName='" + requestName + '\'' +
                '}';
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(String downloadId) {
        this.downloadId = downloadId;
    }
}
