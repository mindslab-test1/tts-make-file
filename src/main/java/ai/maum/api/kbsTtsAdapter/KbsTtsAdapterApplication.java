package ai.maum.api.kbsTtsAdapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KbsTtsAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(KbsTtsAdapterApplication.class, args);
    }

}
