package ai.maum.api.kbsTtsAdapter.controller;

import ai.maum.api.kbsTtsAdapter.common.CommonCode;
import ai.maum.api.kbsTtsAdapter.exception.AdapterException;
import ai.maum.api.kbsTtsAdapter.model.response.KbsExceptionResponse;
import ai.maum.api.kbsTtsAdapter.service.AdabpterService;
import ai.maum.api.kbsTtsAdapter.model.params.KbsTtsRequestParams;
import ai.maum.api.kbsTtsAdapter.model.response.KbsMakeFileResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AdapterController {

    private final AdabpterService service;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public AdapterController(AdabpterService service) {
        this.service = service;
    }

    @RequestMapping(
            value = "makeFile"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
            , method = RequestMethod.POST
    )
    public KbsMakeFileResponse kbsTtsMakeFile(
            @RequestBody KbsTtsRequestParams params
            , HttpServletRequest request
    ) {
        return service.kbsTtsMakeFile(params, request);
    }

    @RequestMapping(
            value = "fileDownload/{fileId}"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public void fileDownload(
            HttpServletRequest request
            , HttpServletResponse response
            , @PathVariable("fileId") String fileId) {
        logger.info("[TTS FileDownload] : " + fileId);
        service.fileDownload(request, response, fileId);
    }


    @RequestMapping(
            value = "fileDownload/{downloadPath}/{fileId}"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public void fileDownload2(
            HttpServletRequest request
            , HttpServletResponse response
            , @PathVariable String downloadPath, @PathVariable("fileId") String fileId) {
        logger.info("[TTS FileDownload] : " + fileId);
        service.fileDownload2(request, response, downloadPath, fileId);
    }

    @ExceptionHandler(AdapterException.class)
    public @ResponseBody
    KbsExceptionResponse throwException(AdapterException exception) {
        logger.error("Adapter Exception!!", exception);
        return new KbsExceptionResponse(exception);
    }

}