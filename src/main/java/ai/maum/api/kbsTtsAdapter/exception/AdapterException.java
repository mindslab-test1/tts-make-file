package ai.maum.api.kbsTtsAdapter.exception;

import org.springframework.http.HttpStatus;

public class AdapterException extends RuntimeException{
    private int status;
    private String message;

    public AdapterException(int status) {
        this.status = status;
        setMessage(status);
    }

    public AdapterException(HttpStatus httpStatus) {
        this.status = httpStatus.value();
        setMessage(status);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessage(int status) {
        this.message = HttpStatus.valueOf(status).toString();
    }

    @Override
    public String toString() {
        return "AdapterException{" +
                "status=" + status +
                ", message='" + message + '\'' +
                "} " + super.toString();
    }
}
