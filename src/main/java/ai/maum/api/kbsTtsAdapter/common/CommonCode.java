package ai.maum.api.kbsTtsAdapter.common;

public class CommonCode {
    public static final String API_URL = "https://dev-api.maum.ai/tts/stream";
//    public static final String API_URL = "http://localhost:8080/tts/stream";

    public static final String DEFAULT_PATH = "/home/ubuntu/kbs_adapter/tts/";
//    public static final String DEFAULT_PATH = "/home/gom/kbs/tts/";
    public static final String FILE_DOWNLOAD_SUB_URL = "/fileDownload";
    public static final String WAV_SUFFIX = ".wav";
    public static final int EXPIRY_DATE = 2;
}
